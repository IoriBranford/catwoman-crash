extends KinematicBody2D

# Member variables
const GRAVITY = 60*16 # pixels/second/second
const COLLISION_STRENGTH = 1
const WALK_FORCE = 30
const MIDAIR_FORCE = 2
const CAMERA_FORCE_X = 15
const CAMERA_FORCE_Y = 6
const WALK_MAX_SPEED = 240
const JUMP_SPEED = 600
const JUMP_MAX_AIRBORNE_TIME = 0.2
const JUMP_DELTA_ANGLE_HORIZONTAL = deg2rad(60)
const FLOOR_NORMAL = Vector2(0,-1)
var velocity = Vector2()
var on_air_time = 100
var jumping = false
var pounced_enemies = []

var prev_jump_pressed = false
var input_dir = Vector2()

signal captured

func is_class(c):
	return c=="Player"

func _physics_process(delta):
	var nextanimation = $AnimationPlayer.current_animation
	input_dir = Vector2()
	if Input.is_action_pressed("move_left"):
		input_dir.x -= 1
	if Input.is_action_pressed("move_right"):
		input_dir.x += 1
	if Input.is_action_pressed("move_up"):
		input_dir.y -= 1
	if Input.is_action_pressed("move_down"):
		input_dir.y += 1
	
	var targetvelx = input_dir.x * WALK_MAX_SPEED
	var forces = Vector2(targetvelx - velocity.x, GRAVITY)
	
	if is_on_floor():
		for enemy in pounced_enemies:
			enemy.set_pounced_by(null)
		pounced_enemies.clear()
		on_air_time = 0
		forces.x *= WALK_FORCE
	else:
		forces.x *= MIDAIR_FORCE

	# Integrate forces to velocity
	velocity += forces * delta
	# Integrate velocity into motion and move
	var newvelocity = move_and_slide(velocity, FLOOR_NORMAL, 60, 4, 1)
	for i in range(0, get_slide_count()):
		var collision = get_slide_collision(i)
		var collider = collision.collider
		if collider.is_class("Decor"):
			if collider.try_knock_down(COLLISION_STRENGTH*velocity, collision):
				newvelocity = velocity
		elif collider.is_class("Enemy"):
			var normal = collision.normal
			var enemyface = collider.get_face_vector()
			var hittop = normal.dot(FLOOR_NORMAL) >= .5
			var vdf = velocity.dot(enemyface)
			var fdn = enemyface.dot(normal)
			var pounced = abs(vdf) > JUMP_SPEED/2
			var hitfront = fdn > 0
			if pounced:
				collider.stun(1)
				collider.set_pounced_by(self)
				pounced_enemies.append(collider)
				newvelocity = velocity
			elif hittop:
				nextanimation = "jump_up"
				jumping = true
				newvelocity = normal*JUMP_SPEED
				collider.stun(1)
			else:
				$AnimatedSprite.flip_h = enemyface.x < 0
				$AnimationPlayer.play("captured")
				emit_signal("captured")
				collider.capture_player(self)
				set_physics_process(false)
				return
	
	velocity = newvelocity
	if has_node("Camera2D"):
		var target_offset = velocity/16
		if is_on_floor() && input_dir.x == 0:
			target_offset.y += 128*input_dir.y
		$Camera2D.offset.x += (target_offset.x - $Camera2D.offset.x)*delta*CAMERA_FORCE_X
		$Camera2D.offset.y += (target_offset.y - $Camera2D.offset.y)*delta*CAMERA_FORCE_Y
	
	if jumping and velocity.y > 0:
		# If falling, no longer jumping
		jumping = false
	
	if input_dir.x != 0:
		$AnimatedSprite.flip_h = input_dir.x < 0
	
	var input_jump = Input.is_action_pressed("jump")
	var input_pounce = Input.is_action_pressed("pounce")
	if on_air_time < JUMP_MAX_AIRBORNE_TIME and (input_jump or input_pounce) and not prev_jump_pressed and not jumping:
		# Jump must also be allowed to happen if the character left the floor a little bit ago.
		# Makes controls more snappy.
		var jump_delta_angle = 0
		if input_pounce || input_dir.y > 0:
			var pounce_dir = 1
			if $AnimatedSprite.flip_h:
				pounce_dir = -1
			velocity.x = WALK_MAX_SPEED*pounce_dir
			jump_delta_angle += pounce_dir * JUMP_DELTA_ANGLE_HORIZONTAL
			nextanimation = "jump_forward"
		else:
			nextanimation = "jump_up"
		velocity += FLOOR_NORMAL.rotated(jump_delta_angle) * JUMP_SPEED
		jumping = true
	
	on_air_time += delta
	prev_jump_pressed = input_jump or input_pounce
	
	if jumping:
		pass
	elif !is_on_floor():
		pass
#		nextanimation = "jump_up"
	elif input_dir.x != 0:
		nextanimation = "crawl"
	else:
		nextanimation = "idle"
	
	if nextanimation != $AnimationPlayer.current_animation:
		$AnimationPlayer.play(nextanimation)
	
#	for body in $grabarea.get_overlapping_bodies():
#		if body.get_class() == "TileMap":
#			check_ledge_grab(body)
	$Polygon2D.position = get_floor_position() - position

func get_floor_position():
	return $floorraycast.get_collision_point()

func check_ledge_grab(tilemap):
	if velocity.y == 0 || input_dir.x == 0:
		return

	var extents = $grabarea/rect.shape.extents
	var rectpos = $grabarea/rect.global_position

	var x
	if input_dir.x < 0:
		x = rectpos.x - extents.x
	else:
		x = rectpos.x + extents.x
	var top_cell = tilemap.world_to_map(Vector2(x, rectpos.y - extents.y))
	var bottom_cell = tilemap.world_to_map(Vector2(x, rectpos.y + extents.y))
	if top_cell == bottom_cell:
		return
	var top_tile = tilemap.get_cellv(top_cell)
	var bottom_tile = tilemap.get_cellv(bottom_cell)
	var tileset = tilemap.tile_set
	if top_tile >= 0:
		top_tile = tileset.tile_get_shape_count(top_tile)
	if bottom_tile >= 0:
		bottom_tile = tileset.tile_get_shape_count(bottom_tile)
	if top_tile <= 0 && bottom_tile > 0:
		if input_dir.x < 0:
			bottom_cell.x += 1
		position = tilemap.map_to_world(bottom_cell)
		velocity.y = 0
		jumping = false
