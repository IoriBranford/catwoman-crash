extends Node2D
const Decor = preload("res://decor/decor.gd")
onready var levelscene = load($level.filename)
var score = 0
var goal = 0
var witnesses = 0

func _ready():
	OS.window_fullscreen = true
	init_game(true)
	get_tree().paused = true

func _input(event):
	if event.is_action_type():
		if event.is_action_pressed("toggle_fullscreen"):
			OS.window_fullscreen = !OS.window_fullscreen
		elif $ui/Title.position.y < get_viewport_rect().size.y:
			if !$ui/AnimationPlayer.is_playing():
				$ui/AnimationPlayer.play("title_drop")
				$ui/Title/start.visible = false

func init_game(first_time):
	$ui/hud.visible = !first_time
	$ui/hud/exitlabel.visible = false
	$ui/results.visible = false
	$ui/victory.visible = false
	
	_on_score_points(-score)
	goal = 0
	witnesses = 0
	
	var binds = []
	get_tree().call_group("Decor", "connect", "score_points", self, "_on_score_points", binds, CONNECT_ONESHOT)
	get_tree().call_group("Enemies", "connect", "witnessed_player", self, "_on_enemy_witnessed_player", binds, CONNECT_ONESHOT)

	var alldecor = get_tree().get_nodes_in_group("Decor")
	Decor.sort(alldecor)
	for i in range(0, len(alldecor)*3/4):
		var decor = alldecor[i]
		goal += decor.score_value
	$ui/hud/goal.text = str(goal)
	
	$level.connect("player_won", self, "_on_level_player_won", binds, CONNECT_ONESHOT)
	$level.connect("player_lost", self, "_on_level_player_lost", binds, CONNECT_ONESHOT)
	$level/map/instructions.visible = !first_time

func restart():
	$winmusic.stop()
	if !$music.playing:
		$music.play()
	$level.queue_free()
	remove_child($level)
	var level = levelscene.instance()
	level.name = "level"
	add_child(level)

	init_game(false)

func _unhandled_key_input(event):
	if event.pressed:
		match event.scancode:
			KEY_R:
				restart()
			KEY_S:
				screenshot()

const FILENAME = "user://{year}-{month}-{day}_{hour}-{minute}-{second}.png"

func screenshot():
	get_viewport().set_clear_mode(Viewport.CLEAR_MODE_ONLY_NEXT_FRAME)
	# Let two frames pass to make sure the screen was captured
	yield(get_tree(), "idle_frame")
	yield(get_tree(), "idle_frame")

	# Retrieve the captured image
	var img = get_viewport().get_texture().get_data()
  
	# Flip it on the y-axis (because it's flipped)
	img.flip_y()
	
	var filename = FILENAME.format(OS.get_datetime(true))
	img.save_png(filename)

func _on_enemy_witnessed_player():
	witnesses += 1

func _on_score_points(points):
	if score < goal && score + points >= goal:
		$goalsound.play()
		$ui/hud/exitlabel.visible = true
		$level.open_exits()
	score += points
	$ui/hud/damage.text = str(score)
	$level/map/instructions.visible = false

func _on_level_player_won():
	$music.stop()
	$winmusic.play()
	if !$ui/AnimationPlayer.is_playing():
		$ui/AnimationPlayer.play("victory")
		$ui/victory.visible = true

func _on_level_player_lost():
	$losetimer.start()

func _on_losetimer_timeout():
	restart()

func _on_AnimationPlayer_animation_finished(anim_name):
	match anim_name:
		"title_drop":
			get_tree().paused = false
			$ui/hud.visible = true
			$level/map/instructions.visible = true
		"victory":
			$ui/victory.visible = true
			var maxwitnesses = len(get_tree().get_nodes_in_group("Enemies"))
			$ui/results.start(score, witnesses, maxwitnesses)
			$ui/hud.visible = false
			$level/exits.visible = false
