extends KinematicBody2D

export var full_speed = 180.0
export var walk_speed = 60.0
export var accel = 240.0
export var min_speed = 30.0
var target_speed = walk_speed
var speed = target_speed
var animated_sprite
var stunned = false
var pounced_by
var stunned_velocity = Vector2()
const GRAVITY = 60*16 # pixels/second/second
const FLOOR_NORMAL = Vector2(0,-1)
const STUN_UNMASK = 32+65536	# Player + DecorBroken
onready var SIGHT_DIST = $playersight.cast_to.x
const KNOCKOUT_FORCE = 240
const MAX_STUN_TIME = 10
var is_witness = false

signal witnessed_player

func get_face_dir():
	if $AnimatedSprite.flip_h:
		return -1
	return 1

func get_face_vector():
	return Vector2(get_face_dir(), 0)

func is_class(c):
	return c=="Enemy"

func _ready():
	if has_node("../AnimatedSprite"):
		animated_sprite = $"../AnimatedSprite"
	else:
		animated_sprite = $AnimatedSprite
	animated_sprite.visible = true
	
func _physics_process(delta):
	if stunned:
		if pounced_by:
			position = pounced_by.position
		else:
			stunned_velocity.y += GRAVITY*delta
			stunned_velocity = move_and_slide(stunned_velocity, FLOOR_NORMAL)
	elif $AnimationPlayer.current_animation != "capture":
		update_movement(delta)

func is_player_sighted():
	var sightedobject = $playersight.get_collider()
	return sightedobject && sightedobject.is_class("Player")
	
func update_movement(delta):
	var playersighted = is_player_sighted()
	if playersighted:
		if $alerttimer.is_stopped():
			target_speed = full_speed
			$alertsound.pitch_scale = 1 + randf()*.5
			$alertsound.play()
			if !is_witness:
				is_witness = true
				emit_signal("witnessed_player")
		$alerttimer.start()
		navigate_player()
		
	$playersight.cast_to.x = SIGHT_DIST*get_face_dir()
	$playersight.cast_to.y = 2*$playersight.position.y * cos($playersight/Timer.time_left * PI * 4)

	if target_speed < speed:
		speed = max(target_speed, speed - accel*delta)
	elif target_speed > speed:
		speed = min(target_speed, speed + accel*delta)
		
	var animation
	if target_speed == full_speed:
		animation = "run"
	else:
		animation = "walk"
	if animation != $AnimationPlayer.current_animation:
		$AnimationPlayer.play(animation)
	
	var pathfollow2d = get_parent()
	var path2d = pathfollow2d.get_parent()
	var lastpos = pathfollow2d.position
	pathfollow2d.offset += speed*delta
	var pathdone = !pathfollow2d.loop && pathfollow2d.offset >= path2d.curve.get_baked_length()
	if pathdone:
		navigate_player()
		if playersighted || !$alerttimer.is_stopped():
			target_speed = full_speed
		else:
			target_speed = walk_speed
	else:
		var directionx = pathfollow2d.position.x - lastpos.x
		animated_sprite.flip_h = directionx < 0

func navigate(pos):
	var navigation = get_tree().get_nodes_in_group("Navigation")
	assert(navigation && navigation[0])
	navigation = navigation[0]
	
	var pathfollow2d = get_parent()
	var pathpoints = navigation.get_simple_path(global_position, pos, true)

	var path2d = pathfollow2d.get_parent()
	path2d.curve.clear_points()
	for point in pathpoints:
		path2d.curve.add_point(point)
	
	if path2d.has_node("Line2D"):
		path2d.get_node("Line2D").points = pathpoints
	
	pathfollow2d.offset = 0
	pathfollow2d.loop = false

func slow_down(slowdown):
	speed = max(min_speed, speed - slowdown)

func set_pounced_by(player):
	pounced_by = player
	$stuntimer.paused = (player != null)
	if player:
		$AnimatedSprite.flip_h = player.get_node("AnimatedSprite").flip_h
		global_position = player.position

func stun(stuntime):
	$hitsound.pitch_scale = 1 + randf()*.5
	$hitsound.play()
	if stunned:
		$stuntimer.wait_time = min($stuntimer.time_left + stuntime, MAX_STUN_TIME)
		$stuntimer.start()
		return
	stunned = true
	var pathfollow2d = get_parent()
	position = pathfollow2d.position
	pathfollow2d.position = Vector2()
	stunned_velocity = Vector2()
	$stuntimer.wait_time = min(stuntime, MAX_STUN_TIME)
	$stuntimer.start()
	collision_mask &= ~STUN_UNMASK
	$AnimationPlayer.play("down")

func decor_hit(decor):
	slow_down(decor.weight/8)
	navigate_player()
	target_speed = full_speed

func _on_head_body_entered(body):
	if body.is_class("Decor"):
		var tohead = $head.global_position - body.position
		var force = body.linear_velocity*body.mass
		if force.dot(tohead.normalized()) > KNOCKOUT_FORCE:
			stun(body.weight/360)
		else:
			speed = min_speed

func _on_stuntimer_timeout():
	$AnimationPlayer.play("getup")
	
func recover():
	stunned = false
	var pathfollow2d = get_parent()
	pathfollow2d.position = position
	position = Vector2()
	collision_mask |= STUN_UNMASK
	speed = min_speed
	target_speed = full_speed
	navigate_player()

func navigate_player():
	var pathfollow2d = get_parent()
	var player = get_tree().get_nodes_in_group("Players")
	assert(player && player[0])
	player = player[0]
	navigate(player.get_floor_position())

func capture_player(player):
	$head/CollisionShape2D.disabled = true
	collision_mask &= ~STUN_UNMASK
	player.position = global_position
	$AnimationPlayer.play("capture")