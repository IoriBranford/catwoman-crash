#!/bin/sh
set -e

# Set to your itch username
ORG=ioribranford

VERSION=$1
if [ ! ${VERSION} ]
then
	echo "Usage: publish-itch.sh <VERSION>"
	exit 1
fi

PROJECT=${PROJECT:=${PWD##*/}}

publish() {
	FILE=$1
	CHANNEL=$2
	if [ -e ${FILE} ]
	then butler push --userversion ${VERSION} ${FILE} ${ORG}/${PROJECT}:${CHANNEL}
	fi
}

publish ${PROJECT}-${VERSION}-windows.zip windows
publish ${PROJECT}-${VERSION}-macosx.zip osx
publish ${PROJECT}-${VERSION}-android.zip android
publish ${PROJECT}-${VERSION}-linux.zip linux
