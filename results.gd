extends Control

export var witness_bonus = 1000
export var no_witnesses_add_bonus = 1000

func start(damage, witnesses, maxwitnesses):
	visible = true
	$damage/amount.text = "$"+str(damage)
	var witnessesbonus = (maxwitnesses-witnesses)*witness_bonus
	if witnesses == 0:
		witnessesbonus += no_witnesses_add_bonus
		$witness.text = "No Witnesses"
		$witness/amount.visible = false
	else:
		$witness.text = "Witnesses"
		$witness/amount.text = str(witnesses)+"/"+str(maxwitnesses)
		$witness/amount.visible = true
	$witness/bonus.text = "$"+str(witnessesbonus)
	$total/amount.text = "$"+str(damage+witnessesbonus)