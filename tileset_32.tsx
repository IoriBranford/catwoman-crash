<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.1" name="tileset_32" tilewidth="32" tileheight="32" tilecount="56" columns="8">
 <image source="tileset_32.png" width="256" height="224"/>
 <tile id="2">
  <objectgroup draworder="index">
   <object id="1" type="navigation" x="0" y="0">
    <polygon points="0,0 0,1 32,1 32,0 31,0 1,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="3">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polygon points="0,0 0,32 32,32 32,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="10">
  <objectgroup draworder="index">
   <object id="1" x="0" y="32">
    <polygon points="0,0 32,0 32,-32"/>
   </object>
   <object id="2" type="navigation" x="0" y="32">
    <polygon points="0,0 0,1 32,-31 32,-32 31,-32 1,-32 0,-32 0,-31 30,-31 -1,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="11">
  <objectgroup draworder="index">
   <object id="1" type="navigation" x="0" y="32">
    <polygon points="0,0 0,1 32,-31 32,-32 31,-32 1,-32 0,-32 0,-31 30,-31 -1,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="18">
  <objectgroup draworder="index">
   <object id="2" x="0" y="32">
    <polygon points="0,0 32,0 32,-32"/>
   </object>
   <object id="7" type="navigation" x="0" y="32">
    <polygon points="-1,0 31,-32 32,-32 32,-31 0,1 0,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="19">
  <objectgroup draworder="index">
   <object id="8" type="one-way" x="9" y="16">
    <polygon points="0,0 0,1 14,1 14,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="22">
  <objectgroup draworder="index">
   <object id="1" x="32" y="0">
    <polygon points="0,0 -16,32 0,32"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="23">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polygon points="0,0 0,32 32,32 32,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="24">
  <objectgroup draworder="index">
   <object id="3" x="0" y="0">
    <polygon points="0,0 0,32 32,32 32,0"/>
   </object>
   <object id="5" type="navigation" x="0" y="0">
    <polygon points="0,0 0,1 32,1 32,0 31,0 1,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="25">
  <objectgroup draworder="index">
   <object id="4" x="0" y="0">
    <polygon points="0,0 0,32 16,32 20,20 32,16 32,0"/>
   </object>
   <object id="6" type="navigation" x="0" y="0">
    <polygon points="0,0 0,1 32,1 32,0 31,0 1,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="26">
  <objectgroup draworder="index">
   <object id="3" x="0" y="0">
    <polygon points="0,0 0,16 32,16 32,0"/>
   </object>
   <object id="5" type="navigation" x="0" y="0">
    <polygon points="0,0 0,1 32,1 32,0 31,0 1,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="27">
  <objectgroup draworder="index">
   <object id="4" type="navigation" x="0" y="32">
    <polygon points="-1,0 31,-32 32,-32 32,-31 0,1 0,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="30">
  <objectgroup draworder="index">
   <object id="1" x="16" y="0">
    <polygon points="0,0 -16,32 16,32 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="33">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polygon points="0,0 0,32 16,32 20,20 32,16 32,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="34">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polygon points="0,0 0,16 32,16 32,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="40">
  <objectgroup draworder="index">
   <object id="2" x="0" y="0">
    <polygon points="0,0 0,32 32,32 32,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="41">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polygon points="0,0 0,32 32,32 32,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="42">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polygon points="0,0 0,32 32,32 32,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="43">
  <objectgroup draworder="index">
   <object id="1" x="0" y="2">
    <polygon points="0,0 0,30 32,30 32,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="44">
  <objectgroup draworder="index">
   <object id="2" x="0" y="2">
    <polygon points="0,0 0,30 32,30 32,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="45">
  <objectgroup draworder="index">
   <object id="2" x="0" y="2">
    <polygon points="0,0 0,30 16,30 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="48">
  <objectgroup draworder="index">
   <object id="1" type="one-way" x="0" y="0">
    <polygon points="0,0 0,1 32,1 32,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="49">
  <objectgroup draworder="index">
   <object id="2" type="one-way" x="0" y="0">
    <polygon points="0,0 0,1 32,1 32,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="50">
  <objectgroup draworder="index">
   <object id="2" type="one-way" x="0" y="0">
    <polygon points="0,0 0,1 26,1 26,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="51">
  <objectgroup draworder="index">
   <object id="1" type="one-way" x="0" y="0">
    <polygon points="0,0 0,1 32,1 32,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="52">
  <objectgroup draworder="index">
   <object id="1" type="one-way" x="0" y="0">
    <polygon points="0,0 0,1 32,1 32,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="53">
  <objectgroup draworder="index">
   <object id="1" type="one-way" x="0" y="0">
    <polygon points="0,0 0,1 32,1 32,0"/>
   </object>
  </objectgroup>
 </tile>
</tileset>
