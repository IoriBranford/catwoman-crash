extends RigidBody2D

export var score_value = 100
export var force_to_knock_down = 0
var broken_texture

const KNOCKED_LAYER = 65536	# DecorBroken
const KNOCKED_MASK = 1		# World
const SLEEP_COLOR_MODULATE = Color(.5, .5, .5)

signal score_points

func is_class(c):
	return c=="Decor"

func _ready():
	if has_node("Sprite"):
		var path = $Sprite.texture.resource_path
		path = path.substr(0, path.rfind(".")) + "_broken.png"
#		if File.new().file_exists(path): # broken by https://github.com/godotengine/godot/issues/24065
		broken_texture = load(path)
	connect("body_entered", self, "_on_decor_body_entered")
	connect("body_exited", self, "_on_decor_body_exited")
	connect("sleeping_state_changed", self, "_on_sleeping_state_changed")
	
func _on_decor_body_entered(body):
	$AudioStreamPlayer2D.pitch_scale = 1 + randf()*.5
	$AudioStreamPlayer2D.play()
	knock_down()
	if body.is_class("Decor"):
		body.knock_down()
	elif body.is_class("Enemy"):
		body.decor_hit(self)

func _on_decor_body_exited(body):
	if body.is_class("Enemy"):
		collision_layer = 0

func try_knock_down(force, collision):
	var normal = collision.normal
	var applied_force = force.dot(normal)
	if -applied_force >= force_to_knock_down:
		knock_down(applied_force*normal)
		return true
	return false
	
func knock_down(force = Vector2()):
	linear_velocity += force
	angular_velocity += force.x/16
	gravity_scale = 1
	collision_layer = KNOCKED_LAYER
	collision_mask = KNOCKED_MASK
	if has_node("Sprite"):
		if broken_texture:
			$Sprite.texture = broken_texture
	if has_node("AnimatedSprite"):
		if $AnimatedSprite.frames.has_animation("broken"):
			$AnimatedSprite.play("broken")
	emit_signal("score_points", score_value)

func _on_sleeping_state_changed():
	if gravity_scale == 1:
		if sleeping:
			sleep()

func sleep():
	if has_node("AnimatedSprite"):
		$AnimatedSprite.self_modulate = SLEEP_COLOR_MODULATE
	if has_node("Sprite"):
		$Sprite.self_modulate = SLEEP_COLOR_MODULATE
	mode = RigidBody2D.MODE_STATIC
	collision_layer = 0
	collision_mask = 0

class Sorter:
	static func compare(a, b):
		return a.score_value > b.score_value

static func sort(array):
	array.sort_custom(Sorter, "compare")
