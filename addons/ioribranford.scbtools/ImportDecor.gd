tool
extends Button

const DECOR_NODE = preload("decor.tscn")

onready var overwrite = $OverwriteCheckButton.pressed

func import_decor(path):
	var decor = DECOR_NODE.instance()
	decor.name = path.get_file().get_basename()
	var decorfile = path.get_basename()+".tscn"
	if !overwrite && File.new().file_exists(decorfile):
		print(decorfile, " already exists and overwrite disabled")
		return
	
	var sprite = decor.get_node("Sprite")
	sprite.texture = load(path)
	var size = sprite.texture.get_size()
	sprite.offset.y = int(size.y/2) % 4
	
	var collisionshape2d = decor.get_node("CollisionShape2D")
	collisionshape2d.position.y = sprite.offset.y
	collisionshape2d.shape = RectangleShape2D.new()
	collisionshape2d.shape.extents = size/2
	
	var packedscene = PackedScene.new()
	packedscene.pack(decor)
	ResourceSaver.save(decorfile, packedscene)
	
	decor.queue_free()
	
	print(decorfile, " created successfully")

func _on_FileDialog_files_selected(paths):
	for path in paths:
		import_decor(path)

func _on_OverwriteCheckButton_toggled(button_pressed):
	overwrite = button_pressed
