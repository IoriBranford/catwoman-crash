extends Navigation2D

var score = 0

signal player_won
signal player_lost

func _ready():
	var camera = $map/player/Camera2D
	var tilemap = $map/bg
	var limit = tilemap.get_used_rect()
	var cellsize = tilemap.get_cell_size()
	limit.size.x *= cellsize.x
	limit.size.y *= cellsize.y
	limit.position.x *= cellsize.x
	limit.position.y *= cellsize.y
	camera.limit_left = limit.position.x
	camera.limit_top = limit.position.y
	camera.limit_right = limit.end.x
	camera.limit_bottom = limit.end.y

func open_exits():
	$exitdoors.collision_layer = 0
	$exitdoors.collision_mask = 0
	$exits.visible = true

func _on_exits_area_entered(area):
	var camera = $map/player/Camera2D
	$map/player.remove_child(camera)
	camera.position = $map/player.position
	add_child(camera)
	emit_signal("player_won")

func _on_player_captured():
	emit_signal("player_lost")
